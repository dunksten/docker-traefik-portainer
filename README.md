# Docker-Traefik-Portainer

Setting up Docker, Portainer and Traefik for easy deployment.

## Table of contents

1. Introduction
2. Tutorial
3. Example code

## 1. Introduction

In this ePortfolio I want to introduce you in easy deployment of containers. Not all of you are familiar with the command line or want to use it.
Luckily there is another way of deploying and controlling docker containers and docker stacks. With Portainer you get a nice web application
from which you can do all the things you have to do normally with the command line. In addition to that, I want to show you a nice way to route all the
traffic to your containers and setup automatic SSL and reverse proxying.

Tired of writing configuration files for your nginx proxy everytime you want to setup a quick test deployment for your newest web application?

I have something for you! Traefik, the easy to setup and easy to control dockerized reverse proxy application. Traefik can control all your frontend
and backend applications and sets up SSL automatically for you. No need to use the command line or write config files. All Traefik routes can be controlled
by labeling the container deployments with the right arguments.

## 2. Tutorial

With this tutorial you will get a complete Docker/Portainer/Traefik stack. Further information about usage can be found in the slides.  
Watch the presentation to see a live demo!

### Prerequisites

**Docker must be installed**

You can for example look in [Ginos ePortfolio](https://softwareengineering.freeforums.net/thread/719/docker-introduction) or do the following steps:  
For Windows you have to get [Docker Desktop for Windows](https://docs.docker.com/docker-for-windows/).  
On Mac you have to install [Docker Desktop for Mac](https://docs.docker.com/docker-for-mac/install/).  
On Linux Ubuntu execute the following code:  
``` sh
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo groupadd docker
sudo usermod -aG docker $USER
```
to install the [Docker engine](https://docs.docker.com/engine/install/).  
For other distros look at [the installation page](https://docs.docker.com/engine/install/).

### Enable Docker Swarm

For using the latest features of portainer and portainer agent, you have to enable Docker swarm.
I will cover only the single node swarm now. For further reading look into the [official documentation](https://docs.docker.com/engine/swarm/).  

To enable swarm on your pc enter the following:  
``` sh
docker swarm init
```

### Create network for traefik

If we want to use a external network for our stack, it has to be created before we deploy our stack.  
To create a network called "public", execute the following:  
``` sh
docker network create public
```

That is it. You now have your standalone docker swarm cluster ready for service deployments.
Continue with the presentation to learn how to deploy the portainer/agent/traefik stack or if you just want a running system, copy the **portainer-agent-traefik.yml** and start it with:  
``` sh
docker stack deploy --compose-file portainer-agent-traefik.yml portainer
```

## 3. Example code

``` yml
version: '3.2'

services:
  agent:
    image: portainer/agent
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/lib/docker/volumes:/var/lib/docker/volumes
    environment:
      AGENT_CLUSTER_ADDR: tasks.portainer_agent
    networks:
      - agent_network
    deploy:
      mode: global
      placement:
        constraints: [node.platform.os == linux]
        
  portainer:
    image: portainer/portainer
    command: -H tcp://tasks.portainer_agent:9001 --tlsskipverify
    volumes:
      - portainer_data:/data
    networks:
      - agent_network
      - public
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints: [node.role == manager]
      labels:
        - traefik.enable=true
        - traefik.http.routers.portainer.entrypoints=web
        - traefik.http.routers.portainer.rule=Host(`portainer.localhost`)
        - traefik.http.services.portainer.loadbalancer.server.port=9000
        - traefik.docker.network=public
      
  traefik:
    image: traefik:latest
    command:
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--entryPoints.web.address=:80"
      - "--entryPoints.websecure.address=:443"
      - "--certificatesresolvers.traefik.acme.httpchallenge.entrypoint=web"
      - "--certificatesresolvers.traefik.acme.httpchallenge=true"
      - "--certificatesResolvers.traefik.acme.email=your-email@your-domain.org"
      - "--certificatesResolvers.traefik.acme.storage=acme.json"
      - "--providers.docker.swarmMode=true"
    ports:
      - target: 80
        published: 80
        mode: host
      - target: 443
        published: 443
        mode: host
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock:ro"
    networks:
      - public
    deploy:
      mode: global
      placement:
        constraints:
          - node.role == manager
      update_config:
        parallelism: 1
        delay: 10s
      labels:
        - traefik.enable=true
        - traefik.docker.network=public
        - traefik.http.services.portainer.loadbalancer.server.port=80

networks:
  agent_network:
    driver: overlay
    attachable: true
  public:
    external: true

volumes:
  portainer_data:

```